//By given an integer value n Write a C Program  to print multiplication tables from 1 to n
#include <stdio.h>
int main (){
	int n, a,b,multiplication;
	printf("Enter a number : ");
	scanf("%d",&n);
	
	for (a=1; a<=n;a++)
	{
	
	printf("Multiplication table of %d\n",a);
	for (b=1; b <=10; ++b)
	   {
	   	   multiplication =a*b;
		  printf("%d*%d = %d \n",a,b,multiplication  );
	   }
	printf("\n");
	}
	return 0;
}
