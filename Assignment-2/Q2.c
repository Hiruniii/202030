// Write a C program that takes a number from the user and checks whether that number is odd or even

#include<stdio.h>

int main()
{
	int number;
	printf("Enter a number");
	scanf("%d",&number);
	
	if(number%2==0)
	{
		printf("%d is a even number",number);
	}
	else
	{
		printf("%d is a odd number",number);
	}
	
	return 0;
}

